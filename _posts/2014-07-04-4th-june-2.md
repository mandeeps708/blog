---
layout: post
title: "4th June"

postday: 2014/07/04
posttime: 11_58
categories: 
- 6-weeks-training
tags: 
- heart-shape-in-latex
- latex
- time-management
---
Today there was another presentation on LaTeX by [Satwant](http://234satwant.wordpress.com/) and Ravina. The presentation was about their task i.e. to work with latex to use different techniques. Hence it was nice presentation. They showed how a text can be formatted to appear in a shape like heart. You can [visit her blog](http://234satwant.wordpress.com/2014/07/02/latex-is-beauty/) to see how to make heart shape. We can include a package to use any other shape. After that there was a seminar by Inderpreet Singh. It was on how to work daily. He explained about how to work constantly and where to work. He added some time management and time tracking techniques. He told us that working at morning (morning birds) is much better than working at night (night owls) and work by making schedule of 25 minutes of work and then break of 5 minutes and so on. But it will take some time to adapt these things. And try to eat, work and sleep at different workspaces.

[ Click Here](http://mandeep7.wordpress.com/2014/07/02/latex-presentation/) to know what is LaTeX.
