---
layout: post
title: "Freeing up a TCP/IP port"

postday: 2015/11/12
posttime: 16_11
categories: 
- 6-month-training
tags: 
- free-up-port-on-linux
- freeing-tcp-port
- fuser-command
- netstat
---
netstat -tulnap can be used to know which port is occupied by which process (program).

To free up a port (for any reason),  you may use this command:
    
    
    fuser -k 8333/tcp
    
    
    
    

You have to replace '8333' with the port number.
