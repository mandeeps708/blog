---
layout: post
title: "4th June"

postday: 2014/06/04
posttime: 14_31
categories: 
- 6-weeks-training
tags: 
- untagged
---
Today I got familiar with QT (pronounced as "cute"), it is a application used to create user interface for other applications. It is a framework that uses programming languages like C++ etc. To install it on my linux 14.04, I first take its setup file from my friend. Its name was like "qt-linux-opensource-5.1.0-x86_64-offline.run". It will not execute as such so to that I changed its permissions for executing it by this command in the terminal (CTRL+ALT+T) :

_chmod 777 qt-linux-opensource-5.1.0-x86_64-offline.run_

Then I was able to directly execute the program by double clicking on it or by writing in the terminal as:

_./qt-linux-opensource-5.1.0-x86_64-offline.run_

Then the setup will begin and by following the simple instructions the qt will be installed on the system.
