---
layout: post
title: "Check copy progress in terminal"

postday: 2015/07/22
posttime: 02_22
categories: 
- linux
tags: 
- progress
- cp-alternative
- rsync
- rsync-cp
- see-copy-progress-in-terminal
---
If we want to check the progress of a copy process in terminal, then we can use the rsync command.

**man rsync:**

_rsync - a fast, versatile, remote (and local) file-copying tool_

rsync works similarly as cp command. For example:
    
    
    rsync source destination

To see the progress, add the --progress flag to rsync
    
    
    rsync --progress source destination

To see it in human-readable format:
    
    
    rsync -ah --progress source destination
