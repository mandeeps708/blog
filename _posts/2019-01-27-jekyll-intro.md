---
layout: post
title: "Setting up Jekyll"

categories:
- jekyll
tags:
- blog
- jekyll
- wordpress
- gitlab
---

So, to set it up, it would be better to set it up on your local computer first.
You will need to install Jekyll and use their template as a quickstart.

https://jekyllrb.com

says to fire up these commands in the terminal:

```bash
$ gem install bundler jekyll

$ jekyll new my-awesome-site

$ cd my-awesome-site

$ bundle exec jekyll serve

# => Now browse to http://localhost:4000
```

This should set up a basic template for your blog with a demo post.

Thanks.