---
layout: post
title: "2nd June"

postday: 2014/06/02
posttime: 13_05
categories: 
- 6-weeks-training
tags: 
- untagged
---
I am pursuing B.Tech from Guru Nanak Dev Engineering College, Ludhiana. I just finished my exams of 2nd year and joined the 6 weeks training from TCC department of the college. Today is the first day of the training. There are two major projects to chose from. One is CAD related and other is LibreHatti (Web related). So I have choose the CAD project. Seniors of the project gave some brief introduction about both the projects.

(Sorry for posting late on the blog because I didn't have the laptop since then.)
