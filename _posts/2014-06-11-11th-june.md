---
layout: post
title: "11th June"

postday: 2014/06/11
posttime: 12_24
categories: 
- 6-weeks-training
tags: 
- untagged
---
Today I have drawn tried some more figures in LibreCAD. I had almost completed the water tank but I forgot to save the code as a file. Okay, So I tried to do some newer commands like "copy" and "rotate" in LibreCAD.

With the help of copy function, we can copy any figure made and paste it anywhere at new coordinates.
    
    
    layer=active.proxy.layerByName("0")
    d=active.document()
    b=Builder(d)
    l=Line(Coord(0,0),Coord(100,100),layer)
    b:append(l):copy(Coord(100,0))
    b:push()
    b:loop(2)
    b:execute()

The output of the above program will be:

![copy command](http://mandeep7.files.wordpress.com/2014/06/copy.png?w=300)

 
