---
layout: post
title: "13th July"

postday: 2015/07/13
posttime: 11_48
categories: 
- 6-month-training
tags: 
- presentation
- reveal-md
---
Today I experimented with reveal-md and started creating the presentation. Also the got the print function working after installing phantomjs by
    
    
    sudo apt-get install phantomjs.

To use the print function:
    
    
    reveal-md presentation.md --print slides.pdf
    
    
    
    

This will create a pdf file named slides.pdf in the current directory but the things got messed up in the pdf file as the background was not properly assigned to the slides and also the transitions are lost.
