---
layout: post
title: "26th June"

postday: 2014/06/27
posttime: 00_35
categories: 
- 6-weeks-training
tags: 
- untagged
---
From the last two days me and my friend [Gurpinder](http://gurpindersingh751.wordpress.com/) were trying to find the solution of our task. We were assigned the work of studying the hatching algorithm. We searched a lot on the internet but we found only videos about graphics(not any example about algorithm) and also we tried to study the hatching file named rs_hatch in LibreCAD v2. But it was very complex and goes over our heads. So we asked for help from [sir](http://hs.raiandrai.com/). And he told us new ways to work and even search on Internet. And our mentor told us to search about hatching algorithms and study the working of that so that it can be implemented in our LibreCAD_v3 project. In the morning, there was a discussion about Souvenir website by [Deepak](http://deekysharma.wordpress.com/). He asked for reviewing the design, colours and other concepts like background, animation etc. from everyone. I, also gave some suggestions like the entire colour theme of a website should be same. He did an excellent work. The website was looking great.
